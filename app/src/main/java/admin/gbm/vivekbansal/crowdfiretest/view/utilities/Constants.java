package admin.gbm.vivekbansal.crowdfiretest.view.utilities;

/**
 * Created by vivekbansal on 11/02/15.
 */
public class Constants {

    public static final int REQUEST_PERMISSION = 100;
    public static final int REQUEST_CAMERA = 101;
    public static final int REQUEST_GALLERY = 102;

}
