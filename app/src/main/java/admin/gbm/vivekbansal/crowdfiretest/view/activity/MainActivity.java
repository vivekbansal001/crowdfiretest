package admin.gbm.vivekbansal.crowdfiretest.view.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import admin.gbm.vivekbansal.crowdfiretest.R;
import admin.gbm.vivekbansal.crowdfiretest.controller.AppController;
import admin.gbm.vivekbansal.crowdfiretest.controller.FireOpenHelper;
import admin.gbm.vivekbansal.crowdfiretest.model.Item;
import admin.gbm.vivekbansal.crowdfiretest.model.listeners.enums.ItemType;
import admin.gbm.vivekbansal.crowdfiretest.view.utilities.Constants;
import admin.gbm.vivekbansal.crowdfiretest.view.utilities.FireAppPreferences;
import admin.gbm.vivekbansal.crowdfiretest.view.utilities.Util;
import admin.gbm.vivekbansal.crowdfiretest.view.widgets.MainPagerAdapter;

public class MainActivity extends Activity implements View.OnClickListener {

    ViewPager mShirtsViewPager, mJeansViewPager;
    ImageView mShuffle, mAddShirts, mAddJeans, mFavComb;

    MainPagerAdapter mShirtPagerAdapter, mJeansPagerAdapter;
    ItemType addItemType;

    AppController mAppController;
    FireOpenHelper mFireOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mShirtsViewPager = (ViewPager) findViewById(R.id.view_pager_shirts);
        mShirtPagerAdapter = new MainPagerAdapter();
        mShirtsViewPager.setAdapter(mShirtPagerAdapter);

        mJeansViewPager = (ViewPager) findViewById(R.id.view_pager_jeans);
        mJeansPagerAdapter = new MainPagerAdapter();
        mJeansViewPager.setAdapter(mJeansPagerAdapter);

        mShuffle = (ImageView) findViewById(R.id.iv_shuffle);
        mShuffle.setOnClickListener(this);

        mFavComb = (ImageView) findViewById(R.id.iv_mark_fav);
        mFavComb.setOnClickListener(this);

        mAddShirts = (ImageView) findViewById(R.id.iv_add_shirt);
        mAddShirts.setOnClickListener(this);

        mAddJeans = (ImageView) findViewById(R.id.iv_add_jeans);
        mAddJeans.setOnClickListener(this);

        mAppController = AppController.getInstance();
        mFireOpenHelper = mAppController.getFireOpenHelper();

        if (FireAppPreferences.isFirstLaunch(this)) {
            Util.startAlarmForEveryMorning(this);
            FireAppPreferences.usedFirst(this);
        }

        if (false && savedInstanceState != null && savedInstanceState.containsKey("isRotate") && savedInstanceState.getBoolean("isRotate")) {
            Log.e("count", "count : " + mFireOpenHelper.getShirtItemCount());

            ArrayList<Item> shirtItems = mFireOpenHelper.getShirtImages();
            ArrayList<Item> jeansItems = mFireOpenHelper.getJeansImages();

            addItemType = ItemType.SHIRTS;
            for (Item shirItem : shirtItems) {
                addViewItemType(Util.getPhoto(shirItem.getImageData()));
            }

            addItemType = ItemType.JEANS;
            for (Item jeansItem : jeansItems) {
                addViewItemType(Util.getPhoto(jeansItem.getImageData()));
            }

        }

    }


    @TargetApi(Build.VERSION_CODES.M)
    private void askForPermission() {

        if (!Util.shouldAskPermission(this)) {
            String[] perm = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
            ActivityCompat.requestPermissions(this, perm, Constants.REQUEST_PERMISSION);
        } else {
            selectImage();
        }
    }

    private void selectImage() {
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.select_from_gallery), getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.take_photo))) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, Constants.REQUEST_CAMERA);
                } else if (items[item].equals(getString(R.string.select_from_gallery))) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, getString(R.string.select_file)), Constants.REQUEST_GALLERY);
                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Constants.REQUEST_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
                } else {
                    Toast.makeText(this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.REQUEST_CAMERA:
                    addViewItemType(Util.selectedCamera(data));
                    break;
                case Constants.REQUEST_GALLERY:
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                        addViewItemType(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    break;
                default:
                    break;
            }
        }
    }


    private void shuffle() {

        Random random = new Random();
        int shirtsCount = mShirtPagerAdapter.getCount();
        int jeansCount = mJeansPagerAdapter.getCount();

        if (shirtsCount == 0 || jeansCount == 0) {
            Toast.makeText(this, "Hey! select image first", Toast.LENGTH_LONG).show();
        } else {
            mShirtsViewPager.setCurrentItem(random.nextInt(shirtsCount));
            mJeansViewPager.setCurrentItem(random.nextInt(jeansCount));
        }

    }

    public void addViewItemType(Bitmap bitmap) {

        ImageView iv = new ImageView(MainActivity.this);
        iv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        iv.setImageBitmap(bitmap);

        if (addItemType.equals(ItemType.SHIRTS)) {
            mShirtPagerAdapter.addView(iv);
            mShirtPagerAdapter.notifyDataSetChanged();
            mShirtsViewPager.setCurrentItem(mShirtPagerAdapter.getCount() - 1);

            mFireOpenHelper.insertShirtImage(bitmap);
        } else {
            mJeansPagerAdapter.addView(iv);
            mJeansPagerAdapter.notifyDataSetChanged();
            mJeansViewPager.setCurrentItem(mJeansPagerAdapter.getCount() - 1);

            mFireOpenHelper.insertJeansImage(bitmap);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.iv_shuffle:
                shuffle();
                break;

            case R.id.iv_mark_fav:
                mFireOpenHelper.insertFavCollection(mShirtPagerAdapter.getItemBitmap(mShirtsViewPager.getCurrentItem()),
                        mJeansPagerAdapter.getItemBitmap(mJeansViewPager.getCurrentItem()));
                break;

            case R.id.iv_add_shirt:
                addItemType = ItemType.SHIRTS;
                askForPermission();
                break;

            case R.id.iv_add_jeans:
                addItemType = ItemType.JEANS;
                askForPermission();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isRotate", true);
        Log.e("count", "count saved: " + mFireOpenHelper.getShirtItemCount());
    }

}
