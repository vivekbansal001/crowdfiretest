package admin.gbm.vivekbansal.crowdfiretest.model.listeners.enums;

/**
 * Created by vivekbansal on 14/03/16.
 */
public enum ItemType {

    SHIRTS(0), JEANS(1);

    private int serial;

    ItemType(int serial) {
        this.serial = serial;
    }

    public static int getItemTypeSerial(ItemType itemType) {
        return itemType.serial;
    }

    public static ItemType getItemType(int serial) {

        switch (serial) {
            case 0:
                return ItemType.SHIRTS;
            case 1:
                return ItemType.JEANS;
            default:
                return ItemType.SHIRTS;
        }

    }
}
