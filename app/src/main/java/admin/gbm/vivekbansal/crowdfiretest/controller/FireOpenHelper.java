package admin.gbm.vivekbansal.crowdfiretest.controller;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;

import java.util.ArrayList;

import admin.gbm.vivekbansal.crowdfiretest.model.Item;
import admin.gbm.vivekbansal.crowdfiretest.view.utilities.DBUtils;
import admin.gbm.vivekbansal.crowdfiretest.view.utilities.Util;


@SuppressLint("SdCardPath")
public class FireOpenHelper extends SQLiteOpenHelper {

    private static String DB_NAME = "FireApp.db";
    private static int DB_VERSION = 1;

    private final Context mContext;


    public FireOpenHelper(Context context) {

        super(context, DB_NAME, null, DB_VERSION);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(DBUtils.CREATE_SHIRT_TABLE);
        db.execSQL(DBUtils.CREATE_JEANS_TABLE);
        db.execSQL(DBUtils.CREATE_FAV_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        update1to2(db);
    }

    private void update1to2(SQLiteDatabase db) {
    }

    public void insertShirtImage(final Bitmap bitmap) {

        new Thread() {
            public void run() {
                SQLiteDatabase database = FireOpenHelper.this.getWritableDatabase();

                ContentValues cv = new ContentValues();
                cv.put(DBUtils.SHIRT_COLUMN_SHIRT_IMAGE, Util.getBytes(bitmap));
                database.insert(DBUtils.TABLE_SHIRT, null, cv);
            }
        }.start();
    }

    public void insertJeansImage(final Bitmap bitmap) {

        new Thread() {
            public void run() {
                SQLiteDatabase database = FireOpenHelper.this.getWritableDatabase();

                ContentValues cv = new ContentValues();
                cv.put(DBUtils.JEANS_COLUMN_JEANS_IMAGE, Util.getBytes(bitmap));
                database.insert(DBUtils.TABLE_JEANS, null, cv);
            }
        }.start();
    }

    public void insertFavCollection(final Bitmap shirtBitmap, final Bitmap jeansBitmap) {

        new Thread() {
            public void run() {
                SQLiteDatabase database = FireOpenHelper.this.getWritableDatabase();

                ContentValues cv = new ContentValues();
                cv.put(DBUtils.FAV_COLUMN_SHIRT_IMAGE, Util.getBytes(shirtBitmap));
                cv.put(DBUtils.FAV_COLUMN_JEANS_IMAGE, Util.getBytes(jeansBitmap));
                database.insert(DBUtils.TABLE_FAV, null, cv);
            }
        }.start();
    }

    public ArrayList<Item> getShirtImages() {

        String query = "select * from " + DBUtils.TABLE_SHIRT;
        ArrayList<Item> items = new ArrayList<Item>();

        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                Item item = new Item();
                item.setId(cursor.getInt(cursor.getColumnIndex(DBUtils.SHIRT_COLUMN_SHIRT_ID)));
                item.setImageData(cursor.getBlob(cursor.getColumnIndex(DBUtils.SHIRT_COLUMN_SHIRT_IMAGE)));
                items.add(item);
            } while (cursor.moveToNext());
        }

        return items;

    }

    public ArrayList<Item> getJeansImages() {

        String query = "select * from " + DBUtils.TABLE_JEANS;
        ArrayList<Item> items = new ArrayList<Item>();

        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                Item item = new Item();
                item.setId(cursor.getInt(0));
                item.setImageData(cursor.getBlob(1));
                items.add(item);
            } while (cursor.moveToNext());
        }

        return items;
    }

    public int getShirtItemCount() {

        String query = "select * from " + DBUtils.TABLE_SHIRT;

        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(query, null);

        if (cursor != null)
            return cursor.getCount();
        else
            return 0;
    }


}
