package admin.gbm.vivekbansal.crowdfiretest.view.widgets;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import admin.gbm.vivekbansal.crowdfiretest.view.utilities.Util;

/**
 * Created by vivekbansal on 14/03/16.
 */
public class DeviceBootReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Util.startAlarmForEveryMorning(context);
        }
    }
}
