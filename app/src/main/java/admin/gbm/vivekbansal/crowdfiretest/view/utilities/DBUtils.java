package admin.gbm.vivekbansal.crowdfiretest.view.utilities;

/**
 * Created by vivekbansal on 14/02/15.
 */
public class DBUtils {


    public static final String TABLE_SHIRT = "Shirts";
    public static final String SHIRT_COLUMN_SHIRT_ID = "shirt_id";
    public static final String SHIRT_COLUMN_SHIRT_IMAGE = "shirt_image";

    public static final String TABLE_JEANS = "Jeans";
    public static final String JEANS_COLUMN_JEANS_ID = "jeans_id";
    public static final String JEANS_COLUMN_JEANS_IMAGE = "jeans_image";

    public static final String TABLE_FAV = "Fav";
    public static final String FAV_COLUMN_SHIRT_IMAGE = "shirt_image";
    public static final String FAV_COLUMN_JEANS_IMAGE = "jeans_image";

    public static final String CREATE_SHIRT_TABLE = "CREATE TABLE IF NOT EXISTS "
            + DBUtils.TABLE_SHIRT + " (" + DBUtils.SHIRT_COLUMN_SHIRT_ID
            + " integer primary key autoincrement, " + DBUtils.SHIRT_COLUMN_SHIRT_IMAGE
            + " blob not null );";

    public static final String CREATE_JEANS_TABLE = "CREATE TABLE IF NOT EXISTS "
            + DBUtils.TABLE_JEANS + " (" + DBUtils.JEANS_COLUMN_JEANS_ID
            + " integer primary key autoincrement, " + DBUtils.JEANS_COLUMN_JEANS_IMAGE
            + " blob not null );";

    public static final String CREATE_FAV_TABLE = "CREATE TABLE IF NOT EXISTS "
            + DBUtils.TABLE_FAV + " (" + DBUtils.FAV_COLUMN_SHIRT_IMAGE
            + " blob not null, " + DBUtils.FAV_COLUMN_JEANS_IMAGE
            + " blob not null );";

}
