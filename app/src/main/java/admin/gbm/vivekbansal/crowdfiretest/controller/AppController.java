package admin.gbm.vivekbansal.crowdfiretest.controller;

import android.app.Application;
import android.content.Context;

import admin.gbm.vivekbansal.crowdfiretest.view.utilities.DeviceResourceManager;


public class AppController extends Application {


    public static final String TAG = AppController.class.getSimpleName();

    private static DeviceResourceManager mDeviceResourceManager;
    private static AppController mInstance;
    private static Context mContext;
    private FireOpenHelper mOpenHelper;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public static DeviceResourceManager getDeviceResourceManager() {
        if (mDeviceResourceManager == null)
            mDeviceResourceManager = new DeviceResourceManager(mContext);

        return mDeviceResourceManager;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mContext = this;
        initDatabase();
    }


    private void initDatabase() {
        getFireOpenHelper();
    }

    public FireOpenHelper getFireOpenHelper() {
        if (mOpenHelper == null)
            mOpenHelper = new FireOpenHelper(this);

        return mOpenHelper;
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        mOpenHelper.close();
    }
}
