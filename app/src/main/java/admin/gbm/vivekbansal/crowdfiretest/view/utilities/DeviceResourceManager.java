package admin.gbm.vivekbansal.crowdfiretest.view.utilities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.view.Display;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.Toast;

public class DeviceResourceManager {

    private static final String LOG_TAG = "DeviceResourceManager";

    private Context mContext = null;
    private Display display = null;
    private float mScreenDinsity = 0.0f;

    public DeviceResourceManager(Context context) {
        mContext = context;
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public int getScreenWidth() {
        if (display == null)
            display = ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        int screenWidth = 0;
        if (Util.hasHoneycombMR1()) {
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
        } else {
            screenWidth = display.getWidth();
        }

        return screenWidth;
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public int getScreenHeight() {
        if (display == null)
            display = ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        int screenHeight = 0;
        if (Util.hasHoneycombMR2()) {
            Point size = new Point();
            display.getSize(size);
            screenHeight = size.y;
        } else {
            screenHeight = display.getHeight();
        }

        return screenHeight;
    }

    public int getStatusBarHeight() {
        int statusBarHeight = 0;
        int resourceId = mContext.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = mContext.getResources().getDimensionPixelSize(resourceId);
        }
        return statusBarHeight;
    }

    public float getScreenDensity() {
        if (mScreenDinsity == 0.0f)
            mScreenDinsity = mContext.getResources().getDisplayMetrics().density;
        return mScreenDinsity;
    }

    public int convertDIPToPX(int dipValue) {
        return Math.round(dipValue * getScreenDensity());
    }

    public int getSwipeMinDistance() {
        return ViewConfiguration.get(mContext).getScaledTouchSlop();
    }

}
