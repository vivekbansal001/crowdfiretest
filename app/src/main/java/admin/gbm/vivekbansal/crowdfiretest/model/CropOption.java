package admin.gbm.vivekbansal.crowdfiretest.model;

import android.content.Intent;
import android.graphics.drawable.Drawable;

/**
 * Created by vivekbansal on 14/03/16.
 */
public class CropOption {

    public CharSequence title;
    public Drawable icon;
    public Intent appIntent;

}
