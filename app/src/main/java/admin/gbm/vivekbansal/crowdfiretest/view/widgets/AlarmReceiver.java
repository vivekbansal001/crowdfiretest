package admin.gbm.vivekbansal.crowdfiretest.view.widgets;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import admin.gbm.vivekbansal.crowdfiretest.view.utilities.Util;

/**
 * Created by vivekbansal on 14/03/16.
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Alarm...", Toast.LENGTH_LONG).show();
        Util.showNotification(context);
    }
}
