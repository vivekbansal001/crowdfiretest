package admin.gbm.vivekbansal.crowdfiretest.model;

/**
 * Created by vivekbansal on 14/03/16.
 */
public class Item {

    int id;
    byte[] imageData;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }
}

