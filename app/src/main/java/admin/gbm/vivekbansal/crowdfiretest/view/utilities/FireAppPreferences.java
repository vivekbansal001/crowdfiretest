package admin.gbm.vivekbansal.crowdfiretest.view.utilities;

import android.content.Context;
import android.content.SharedPreferences;

public class FireAppPreferences {

    private static final String PREFS_NAME = "FireApp";

    private static final String IS_FIRST_START = "is_first_start";


    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static void usedFirst(Context context) {
        getSharedPreferences(context).edit().putBoolean(IS_FIRST_START, false).commit();
    }

    public static boolean isFirstLaunch(Context context) {
        return getSharedPreferences(context).getBoolean(IS_FIRST_START, true);
    }


}
