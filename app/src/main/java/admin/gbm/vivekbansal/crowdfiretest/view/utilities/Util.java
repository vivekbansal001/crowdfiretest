package admin.gbm.vivekbansal.crowdfiretest.view.utilities;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import admin.gbm.vivekbansal.crowdfiretest.R;
import admin.gbm.vivekbansal.crowdfiretest.view.activity.MainActivity;
import admin.gbm.vivekbansal.crowdfiretest.view.widgets.AlarmReceiver;

public class Util {

    final public static boolean IS_DEVELOPMENT = true;
    final private static String LOG_TAG = "FireApp";

    public static boolean hasFroyo() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
    }

    public static boolean hasGingerbread() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
    }

    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    public static final boolean hasHoneycombMR2() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2;
    }

    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }

    public static void v(String msg) {
        if (IS_DEVELOPMENT) {
            Log.v(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
        }
    }

    public static void i(String msg) {
        if (IS_DEVELOPMENT) {
            Log.i(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
        }
    }

    public static void w(String msg) {
        if (IS_DEVELOPMENT) {
            Log.w(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
        }
    }

    public static void d(String msg) {
        if (IS_DEVELOPMENT) {
            Log.d(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
        }
    }

    public static void e(String msg) {
        Log.e(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
    }

    public static void e(String msg, Exception e) {
        Log.e(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg, e);
    }


    public static void startAlarmForEveryMorning(Context context) {

        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        int interval = 24 * 60 * 60 * 1000;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 6);
        calendar.set(Calendar.MINUTE, 0);

        manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), interval, pendingIntent);
    }

    public static void showNotification(Context context) {

        int smalIcon = R.mipmap.ic_launcher;
        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.BigTextStyle notificationBig = new NotificationCompat.BigTextStyle();
        notificationBig.bigText(context.getString(R.string.notification_desc));
        notificationBig.setBigContentTitle(context.getString(R.string.notification_title));

        Intent myIntent = getDefaultIntent(context);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setContentText(context.getString(R.string.notification_desc)).setContentTitle(context.getString(R.string.notification_title)).setSmallIcon(smalIcon).setAutoCancel(true)
                .setTicker(context.getString(R.string.notification_title)).setLargeIcon(largeIcon).setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                .setContentIntent(pendingIntent).setStyle(notificationBig);

        Notification notification = notificationBuilder.build();
        notificationManager.notify(0, notification);

    }

    private static Intent getDefaultIntent(Context context) {

        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        return intent;

    }

    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }

    public static Bitmap getPhoto(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    public static boolean shouldAskPermission(Context context) {

        boolean hasPermission = (ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) && !hasPermission;
    }

    public static Bitmap selectedCamera(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return thumbnail;
    }

}
